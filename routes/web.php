<?php
Route::get('/', function () { return redirect('/admin/home'); });

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('triplogin', 'Auth\TripLoginController@triplogin')->name('auth.trip_login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

$this->get('trip_login', 'Auth\TripLoginController@showTripLoginForm')->name('auth.trip_login');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

// Registration Routes..
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

$this->get('invitation/{invitation_token}', 'Auth\RegisterController@processInvitation')->name('auth.invitation');



    Route::group(['prefix' => 'trip', 'as' => 'trip.'], function () {
        Route::get('home', 'TripController@index');
        Route::get('tours', 'TripController@tourListing');
        Route::match(['get','post'],'feedback/{id}', 'TripController@feedback');
        Route::match(['get','post'],'feedback/{id}/{notid}', 'TripController@feedback');
        Route::get('survey', 'TripController@surveyListing');
        
    });
    Route::group(['middleware' => ['auth', 'check_invitation'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    
    Route::get('surveys', 'Admin\QuestionnaireController@surveys');
    Route::get('survey', 'Admin\QuestionnaireController@surveyListing');
    Route::get('tours', 'Admin\ToursController@tourListing');
    
    Route::match(['get','post'],'createsurvey', 'Admin\QuestionnaireController@createSurvey');
    
    Route::resource('questionnaires', 'Admin\QuestionnaireController');
    Route::post('questionnaires_mass_destroy', ['uses' => 'Admin\QuestionnaireController@massDestroy', 'as' => 'questionnaires.mass_destroy']);
	Route::get('getquesans', 'Admin\QuestionnaireController@getAnswers');
    Route::match(['get','post'],'survey/edit/{id}', 'Admin\QuestionnaireController@surveyEdit');
    Route::match(['get','post'],'survey/view/{id}', 'Admin\QuestionnaireController@surveyView');
	
	Route::match(['get','post'],'questionnaires/create/{id}', 'Admin\QuestionnaireController@add');
	Route::match(['get','post'],'questionnaires/edit/{id}', 'Admin\QuestionnaireController@edit');
    Route::get('deletequestionnaire/{id}', 'Admin\QuestionnaireController@delete');
    Route::get('deletesurvey/{id}', 'Admin\QuestionnaireController@deleteSurvey');
    
    Route::match(['get','post'],'profile', 'Admin\UsersController@profile');
    Route::match(['get','post'],'feedback/{id}', 'Admin\QuestionnaireController@feedback');
    Route::match(['get','post'],'feedback/{id}/{notid}', 'Admin\QuestionnaireController@feedback');
    
    
    
	//Route::match(['get','post'],'surveys', 'Admin\QuestionnaireController@questionnaires');
    
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('properties', 'Admin\PropertiesController');
    Route::post('properties_mass_destroy', ['uses' => 'Admin\PropertiesController@massDestroy', 'as' => 'properties.mass_destroy']);
    Route::post('properties_restore/{id}', ['uses' => 'Admin\PropertiesController@restore', 'as' => 'properties.restore']);
    Route::delete('properties_perma_del/{id}', ['uses' => 'Admin\PropertiesController@perma_del', 'as' => 'properties.perma_del']);
    Route::resource('documents', 'Admin\DocumentsController');
    Route::post('documents_mass_destroy', ['uses' => 'Admin\DocumentsController@massDestroy', 'as' => 'documents.mass_destroy']);
    Route::post('documents_restore/{id}', ['uses' => 'Admin\DocumentsController@restore', 'as' => 'documents.restore']);
    Route::delete('documents_perma_del/{id}', ['uses' => 'Admin\DocumentsController@perma_del', 'as' => 'documents.perma_del']);
    Route::resource('notes', 'Admin\NotesController');
    Route::post('notes_mass_destroy', ['uses' => 'Admin\NotesController@massDestroy', 'as' => 'notes.mass_destroy']);
    Route::post('notes_restore/{id}', ['uses' => 'Admin\NotesController@restore', 'as' => 'notes.restore']);
    Route::delete('notes_perma_del/{id}', ['uses' => 'Admin\NotesController@perma_del', 'as' => 'notes.perma_del']);

    Route::model('messenger', 'App\MessengerTopic');
    Route::get('messenger/inbox', 'Admin\MessengerController@inbox')->name('messenger.inbox');
    Route::get('messenger/outbox', 'Admin\MessengerController@outbox')->name('messenger.outbox');
    Route::resource('messenger', 'Admin\MessengerController');

    Route::resource('tenants', 'Admin\TenantsController');
 
});