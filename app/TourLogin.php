<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Tour as Authenticatable;

/**
 * Class Role
 *
 * @package App
 * @property string $title
*/
class TourLogin extends Authenticatable
{   
    protected $table = 'tour_login';
    public function __construct()
    {
       
    }      
    
}
