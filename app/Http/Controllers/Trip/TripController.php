<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Vendor;
use App\Tour;
use App\QuestionAnswer;
use App\TourSurvey;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('trip.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        DB::enableQueryLog();
        $uid = auth()->user()->id;
        $user = User::findOrFail($uid);
        $tours = array();
        $active_surveys = array();
        $attemptedsurveys = [0];
        $assignedVendors = [0];
        
        $vendors = $user->vendor()->pluck('vendor_id');
        foreach($vendors as $vendorsval){
            $vendor = Vendor::findOrFail($vendorsval);
            $assignedVendors[] = $vendor->vendor_code;
        }
        $tours = Tour::whereIn('master_vendor', $assignedVendors)
                ->orWhereIn('sub_vendor', $assignedVendors)
                ->limit(3)
                ->get();
       
        $attempted_surveys = QuestionAnswer::where(['user_id'=>$uid])->select('tour_survey_id')->get();
        
        foreach($tours as $tour){
            $active_suvey = TourSurvey::where('tour_survey.tour_id', '=', $tour->id)
            ->leftJoin('tours', 'tours.id', '=', 'tour_survey.tour_id')
            ->select('tour_survey.id', 'tours.tour_year', 'tours.tour_record', 'tours.tour_id', 'tour_survey.survey_id')
            ->whereNotExists(function($query) use ($uid)
                {
                    $query->select(DB::raw('question_answer.tour_survey_id'))
                          ->from('question_answer')
                          ->whereRaw('tour_survey.id = question_answer.tour_survey_id')
                          ->whereRaw('question_answer.user_id = '.$uid);
                })->get();
            foreach($active_suvey as $as){
                if(!empty($as->tour_id)){
                    $active_surveys[] = $as;
                }
            }
            
        }   
         //echo '<pre>'; print_r($tours); die;     
        return view('home', ['active_surveys'=>$active_surveys, 'attempted_surveys'=>$attempted_surveys]);
    }
}
