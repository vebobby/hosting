<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use App\User;

class TripLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

use AuthenticatesUsers;
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/customer/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    public function showTripLoginForm()
    {
        return view('auth.triplogin');
    }
	public function triplogin(Request $request)
    {
        $this->validateTripLogin($request);

        if ($this->attemptTripLogin($request)) {

            return $this->sendTripLoginResponse($request);
        }

  
	
        return $this->sendFailedTripLoginResponse($request);
    }

    protected function validateTripLogin(Request $request)
    {
        $this->validate($request, [
            'tour_id' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    protected function attemptTripLogin(Request $request)
    {
		//echo '<pre>'; print_r($this->trip_credentials($request)); die;
        return Auth::guard('trip')->attempt($this->trip_credentials($request));
    }

    protected function trip_credentials(Request $request)
    {
        return $request->only($this->trip_username(), 'password');
    }

    protected function sendTripLoginResponse(Request $request)
    {
        $user = array();
		//echo '<pre>'; print_r(Auth::guard('trip')->user()); die;
        $request->session()->regenerate();

        return redirect('trip/home');
    }

    protected function sendFailedTripLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            'trip_id' => [trans('auth.failed')],
        ]);
    }

    public function trip_username()
    {
        return 'tour_id';
    }

    public function guard()
    {
        return Auth::guard('trip');
    }

    
}
