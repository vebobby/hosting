<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Questionnaire;
use App\Notifications\SurveyAssigned;
use App\Survey;
use App\QuestionAnswer;
use App\TourSurvey;
use App\Tour;
use App\Vendor;
use App\VendorUser;
use Mail;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;


class QuestionnaireController extends Controller
{
    public function index(Request $request)
    {
        $questionnaires = Questionnaire::all();
        return view('admin.survey.questionnaire',['questionnaires'=>$questionnaires]);
    }

    public function surveys(Request $request)
    {
         
        //echo '<pre>'; print_r($attempted_surveys); die; 
        $surveys = Survey::all();
        return view('admin.survey.surveylisting',['surveys'=>$surveys]);
    }

    public function surveyListing(Request $request)
    {
        $uid = auth()->user()->id;
        $user = User::findOrFail($uid);
        $tours = array();
        $surveys = array();
        $attemptedsurveys = [0];
        $assignedVendors = [0];
        
        $vendors = $user->vendor()->pluck('vendor_id');
        foreach($vendors as $vendorsval){
            $vendor = Vendor::findOrFail($vendorsval);
            $assignedVendors[] = $vendor->vendor_code;
        }
        $tours = Tour::whereIn('master_vendor', $assignedVendors)
        ->orWhereIn('sub_vendor', $assignedVendors)
        ->get();

        $attempted_surveys = QuestionAnswer::where(['user_id'=>$uid])->select('tour_survey_id')->get();

        foreach($tours as $tour){
            $active_suvey = TourSurvey::where('tour_survey.tour_id', '=', $tour->id)
            ->leftJoin('tours', 'tours.id', '=', 'tour_survey.tour_id')
            ->select('tour_survey.id', 'tours.tour_year', 'tours.tour_record', 'tours.tour_id', 'tour_survey.survey_id')
            ->orderBy('tour_survey.id', 'desc')
            ->get();
            foreach($active_suvey as $as){
                if(!empty($as->tour_id)){
                    $surveys[] = $as;
                }
            }
            
        }  
       
        return view('admin.survey.surveylisting',['surveys'=>$surveys]);
    }

    
    public function createSurvey(Request $request) {
        $tours = \App\Tour::get()->pluck('tour_id', 'id');
        if($request->method() == 'POST') {
            $question_types = [1=>'subqsn', 2=>'multiple_question', 3=>'single_input', 4=>'textarea_input', 5=>'regular'];
            $survey = new Survey();
            $survey->survey_name  =  Input::get('survey_name');
            if($survey->save()){
                $survey->tour()->sync(array_filter((array)$request->input('tour')));
                $questionnaire = new Questionnaire();
                $qsns = Input::get('qsn');
                $question_type = Input::get('question_type');
                foreach($question_type as $qk => $val){
                   $allquestiontype[$qk] = $question_types[$val[0]];
                }
                foreach($qsns as $k => $qsn){
                    if(isset($qsn)){
                        $qtype = Input::get($allquestiontype[$k]);
                        $newarr[$k]['question'] = $qsn;
                        $newarr[$k]['question_type'] = $allquestiontype[$k];
                        if(isset($qtype[$k])){
                            $newarr[$k]['subqsn'] = $qtype[$k];
                        }else{
                            $newarr[$k]['subqsn'] = '';
                        }
                    }
                }
                $questionnaire->question  =   json_encode($newarr);
                $questionnaire->survey_id  =  $survey->id;
                if($questionnaire->save()){
                    $vendors = [];
                    $totaltours = $request->input('tour');
                    $surveyid = $survey->id;
                    foreach($totaltours as $tourid){
                        $TourSurvey = TourSurvey::where(['tour_survey.tour_id'=>$tourid, 'tour_survey.survey_id'=>$surveyid])
                        ->leftJoin('tours', 'tours.id', '=', 'tour_survey.tour_id')
                        ->select('tour_survey.id', 'tours.tour_id', 'tours.master_vendor', 'tours.sub_vendor')
                        ->first();
                        $vendors[] = $TourSurvey->master_vendor;
                        $vendors[] = $TourSurvey->sub_vendor;

                        $vendor = Vendor::whereIn('vendor_code', $vendors)->get();
                        foreach($vendor as $val){
                            $vendorobj = Vendor::findOrFail($val->id);
                           $users = $vendorobj->users()->pluck('user_id')->toArray();
                           foreach($users as $userval){
                            $userobj = User::findOrFail($userval);
                            $SurveyDetail = Survey::where(['id'=>$surveyid])->first();
                            $surveyobj['TourSurvey'] = $TourSurvey;
                            $surveyobj['survey'] = $SurveyDetail;
                            $surveyobj['creator'] = auth()->user();
                            $userobj->notify(new SurveyAssigned($surveyobj));
                           }
                        }
                    }
                    return redirect('/admin/survey')->with('success',"The survey has been successfully added."); 
                } else{
                    return redirect('/admin/survey')->with('error',"An error has occurred. Please try again later."); 
                }
            }
        }
       
        return view('admin.survey.addsurvey', compact('tours'));
    }
	
	public function getAnswers(Request $request)
    {
        $questionnaires = Questionnaire::all();
		$teachers = DB::table('student_questionnaires_answer')->leftJoin('appointement_booking', 'student_questionnaires_answer.appointment_id', '=', 'appointement_booking.id')->leftJoin('users', 'users.id', '=', 'appointement_booking.student_id')->select('users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.contact_no', 'student_questionnaires_answer.*')->paginate(15);
		//echo '<pre>'; print_r($teachers); die;
        return view('admin.questionnaire_reply',['questionnaires'=>$questionnaires, 'teachers'=>$teachers]);
    }

    public function feedback(Request $request,$id,$notid=null) {
        $uid = Auth::user()->id;
        $qsnans = QuestionAnswer::where(['tour_survey_id'=>$id, 'user_id'=>$uid])->first();
        //echo '<pre>'; print_r($qsnans); die;
        $userDetails = Auth::user();
        $tour_survey_id = TourSurvey::findOrFail($id);
        $tour = Tour::findOrFail($tour_survey_id->tour_id);
        $survey = DB::table('surveys')
                ->leftJoin('questionnaires', 'surveys.id', '=', 'questionnaires.survey_id')
                ->where(['questionnaires.survey_id'=>$tour_survey_id->survey_id])
                ->first();
        $notification = auth()->user()->notifications()->where('id', $notid)->first();

        if ($notification) {
            $notification->markAsRead();
        }            
        if($request->method() == 'POST') {
            //echo '<pre>'; print_r($_POST); die;
            $feedback = Input::get('feedback');
            $comment = Input::get('comment');
            $newarr = ['feedback'=>$feedback, 'comment'=>$comment];
            //echo '<pre>'; print_r($newarr); die;
            // $qsn_review = Input::get('qsn_review');
            // $subqsns = Input::get('subqsn');
            // $subqsn_review = Input::get('subqsn_review');
            // foreach($qsns as $k => $qsn){
            //     $newsubqsn_review = array();
            //     if(isset($subqsn_review[$k])){
            //         foreach($subqsn_review[$k] as $s => $subquestions){
            //             $newsubqsn_review[$k][] = $subquestions[0];
            //         }
            //     }
            //     $newarr[$k]['question'] = $qsn;
            //     $newarr[$k]['question_review'] = $qsn_review[$k];
            //     if(isset($subqsns[$k])){
            //         $newarr[$k]['sub_question'] = $subqsns[$k];
            //     }else{
            //         $newarr[$k]['sub_question'] = '';
            //     }
            //     if(isset($newsubqsn_review[$k])){
            //         $newarr[$k]['sub_question_review'] = $newsubqsn_review[$k];
            //     }else{
            //         $newarr[$k]['sub_question_review'] = '';
            //     }
            // }
           $QuestionAnswer = new QuestionAnswer();
           $QuestionAnswer->answer  =  json_encode($newarr);
           $QuestionAnswer->user_id  =  $uid;
           $QuestionAnswer->tour_survey_id  =  $id;
           if($QuestionAnswer->save()) {
               $emaildata['name'] = Auth::user()->name;
               $emaildata['email'] = Auth::user()->email;
               $emaildata['feedback'] = $feedback;
               $emaildata['comment'] =  $comment;
               $emaildata['tour_place'] = Input::get('tour_place');
               $emaildata['tour_date'] = Input::get('tour_date');
               $emaildata['tour_driver'] = Input::get('tour_driver');
               $emaildata['tour_bus_no'] = Input::get('tour_bus_no');
               //echo '<pre>'; print_r($emaildata); die;
               // Send the email when survey successfully submitted 
					Mail::send('admin.survey.survey_feedback_email', ['survey_feedback' => $emaildata], function($message) use ($userDetails) {
						$message->to($userDetails->email, $userDetails->name)->subject('Survey Submited Successfully');
						$message->from('testvirtual8@gmail.com', 'Holiday');
                    });
                 
                return redirect('admin/home/')->with('success',"Survey was successfully saved."); 
           }
        }
        return view('admin.survey.questionnaire_feedback',['survey'=>$survey, 'tour'=>$tour, 'qsnans'=>$qsnans ]);
    }

    public function add(Request $request, $id) {
        if($request->method() == 'POST') {
            $questionnaire = new Questionnaire();
            $qsns = Input::get('qsn');
            $subqsns = Input::get('subqsn');
            foreach($qsns as $k => $qsn){

                $newarr[$k]['question'] = $qsn;
                if(isset($subqsns[$k])){
                    $newarr[$k]['sub_question'] = $subqsns[$k];
                }else{
                    $newarr[$k]['sub_question'] = '';
                }

            }
            $questionnaire->question  =   json_encode($newarr);
            $questionnaire->survey_id  =  Input::get('survey_id');
            if($questionnaire->save()){
                return redirect('/admin/survey/view/'.$id)->with('success',"The question has been successfully added."); 
            } else{
                return redirect('/admin/survey/view/'.$id)->with('error',"An error has occurred. Please try again later."); 
            }
        }
       
        return view('admin.survey.addquestionnaire', ['s_id'=>$id]);
    }

    
    public function surveyEdit(Request $request,$sid) {
        $tours = \App\Tour::get()->pluck('tour_id', 'id');
        $getSurvey = Survey::findOrFail($sid);
        $survey = DB::table('surveys')
                ->leftJoin('questionnaires', 'surveys.id', '=', 'questionnaires.survey_id')
                ->where(['questionnaires.survey_id'=>$sid])
                ->first();
        if($request->method() == 'POST') {
            $getSurvey->survey_name  =  Input::get('survey_name');
            if($getSurvey->save()){
                $getSurvey->tour()->sync(array_filter((array)$request->input('tour')));
                $question_types = [1=>'subqsn', 2=>'multiple_question', 3=>'single_input', 4=>'textarea_input', 5=>'fill_in_blank'];
                     $questionnaire = new Questionnaire();
                     $qsns = Input::get('qsn');
                     $question_type = Input::get('question_type');
                     foreach($question_type as $qk => $val){
                        $allquestiontype[$qk] = $question_types[$val[0]];
                     }
                     
                     foreach($qsns as $k => $qsn){
                         if(isset($qsn)){
                             $qtype = Input::get($allquestiontype[$k]);
                             $newarr[$k]['question'] = $qsn;
                             $newarr[$k]['question_type'] = $allquestiontype[$k];
                             if(isset($qtype[$k])){
                                 $newarr[$k]['subqsn'] = $qtype[$k];
                             }else{
                                 $newarr[$k]['subqsn'] = '';
                             }
                         }
     
                     }
            $updateQuestions= array(
                'question'=>json_encode($newarr)
            );

            $return = Questionnaire::where(['survey_id' => $sid])->update($updateQuestions);
            if($return){
                return redirect('/admin/survey')->with('success',"The survey was successfully updated."); 
            } else{
                return redirect('/admin/survey')->with('error',"An error has occurred. Please try again later.");  
            }
            
        }
       
        }
        return view('admin.survey.editsurvey', compact('survey','getSurvey','tours'));
    }

    public function surveyView(Request $request,$sid) {
        $questions = Questionnaire::where(['survey_id'=>$sid])->get();
        return view('admin.survey.view_survey', ['questions'=>$questions, 's_id'=>$sid]);
    }

    

    public function edit(Request $request,$qid) {
        $questionnaireData = Questionnaire::where(['id'=>$qid])->first();
        if($request->method() == 'POST') {
            $questionnaire = Questionnaire::find($qid);
            $questionnaire->question  =  Input::get('question');
            $s_id = $questionnaire->survey_id;
            $questionnaire->answer   =  json_encode(Input::get('answer'));
            if($questionnaire->save()){
                return redirect('/admin/survey/view/'.$s_id)->with('success',"The question was successfully updated."); 
            } else{
                return redirect('/admin/survey/view/'.$s_id)->with('error',"An error has occurred. Please try again later."); 
            }
        }
        return view('admin.survey.editquestionnaire', ['questionnaireData'=>$questionnaireData]);
    }

    public function delete(Request $request,$qid) {
        if(!empty($qid)) {
            DB::table('questionnaires')->where('id', '=', $qid)->delete();
            $request->session()->flash('success', "Question deleted successfully.");
        }
        echo 'success';exit;
    }

    public function deleteSurvey(Request $request,$sid) {
        if(!empty($sid)) {
            DB::table('surveys')->where('id', '=', $sid)->delete();
            $request->session()->flash('success', "Survey deleted successfully.");
        }
        echo 'success';exit;
    }

    

    public function questionnaires(Request $request)
	{
		$userId = Auth::id();
		$appointments = [];
		$teacherappointment= AppointmentBooking::where(['teacher_id' => $userId])->get();
		foreach($teacherappointment as $val){
			$appointments[] = $val->id;
		}
		$questionnaires = DB::table('student_questionnaires_answer')
						  ->whereIn('appointment_id', $appointments)
						  ->get();
		return view('admin.student_feedbacks',['questionnaires'=>$questionnaires]);
	}
}