<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('user_access')) {
            return abort(401);
        }

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('user_create')) {
            return abort(401);
        }

        $roles = \App\Role::get()->pluck('title', 'id');
        if (Gate::allows('customer_create')) {
            $roles = \App\Role::where('id', 3)->get()->pluck('title', 'id');
       }
      // echo '<pre>'; print_r($roles); die;

        return view('admin.users.create', compact('roles'));
    }

    public function profile(Request $request)
    {
       $userId = Auth::id();
       $user = User::find($userId);
       if($request->method() == 'POST'){
           if(!empty($_FILES['profilepic']['name'])){
            $file = $_FILES['profilepic']['tmp_name'];  // the path for the uploaded file chunk 
            $fileName = time().$_FILES['profilepic']['name'];  // you receive the file name as a separate post data
            $fileSize = $_FILES['profilepic']['size'];  // you receive the file size as a separate post data
            $targetDir = base_path('public/profilepic');
                if (!file_exists($targetDir)) {
                    @mkdir($targetDir);
                }
            $targetFile = $targetDir.'/'.$fileName;
            
            if(move_uploaded_file($file, $targetFile)) {
                $profileimage = $fileName;

                
            }
            }else{
            
                $profileimage = '';
            
            }
            $user->name = Input::get('username');
            $user->email = Input::get('email');
            $user->profile_image = $profileimage;
            if($user->save()){
                return redirect('/admin/profile')->with('success',"The question has been successfully added."); 
            } else{
                return redirect('/admin/profile')->with('error',"An error has occurred. Please try again later."); 
            }
           

        }  
        return view('admin.users.profile', ['user_info'=>$user]);
    }

    

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (! Gate::allows('user_create')) {
            return abort(401);
        }

        $user = User::create($request->all());
        $user->role()->sync(array_filter((array)$request->input('role')));

        return redirect()->route('admin.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('user_edit')) {
            return abort(401);
        }

        $roles = \App\Role::get()->pluck('title', 'id');
        
        //echo 'pre>'; print_r($master_vendors); die;
        $user  = User::findOrFail($id);

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (! Gate::allows('user_edit')) {
            return abort(401);
        }
        $vendors = array();
        $user = User::findOrFail($id);
        $user->update($request->all());
        $user->role()->sync(array_filter((array)$request->input('role')));
        //$vendors = array_merge((array)$request->input('master_vendors'),(array)$request->input('sub_vendors'));
        //$user->vendor()->sync(array_filter($vendors));
       // $user->sub_vendor()->sync(array_filter((array)$request->input('sub_vendors')));        
        return redirect()->route('admin.users.index');
    }


    /**
     * Display User.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('user_view')) {
            return abort(401);
        }

        $roles     = \App\Role::get()->pluck('title', 'id');
        $notes     = \App\Note::where('user_id', $id)->get();
        $documents = \App\Document::where('user_id', $id)->get();
        $user      = User::findOrFail($id);

        return view('admin.users.show', compact('user', 'notes', 'documents'));
    }


    /**
     * Remove User from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('user_delete')) {
            return abort(401);
        }

        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('user_delete')) {
            return abort(401);
        }

        if ($request->input('ids')) {
            $entries = User::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}