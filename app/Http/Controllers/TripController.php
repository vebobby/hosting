<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Vendor;
use Illuminate\Support\Facades\Input;
use App\Tour;
use Illuminate\Support\Facades\Auth;
use App\QuestionAnswer;
use App\TourSurvey;
use Mail;
use Illuminate\Support\Facades\DB;


class TripController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('auth:trip')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo '<pre>'; print_r(Auth::user()); die;
        DB::enableQueryLog();

        $tour_id = Auth::guard('trip')->user()->tour_id;
        $uid = Auth::guard('trip')->user()->user_id;
        $user = User::findOrFail($uid);
        $tours = array();
        $active_surveys = array();
        $attemptedsurveys = [0];
        $assignedVendors = [0];
        
        $vendors = $user->vendor()->pluck('vendor_id');
        foreach($vendors as $vendorsval){
            $vendor = Vendor::findOrFail($vendorsval);
            $assignedVendors[] = $vendor->vendor_code;
        }
        $tours = Tour::where('tour_id', $tour_id)->get();
       
        $attempted_surveys = QuestionAnswer::where(['user_id'=>$uid])->select('tour_survey_id')->get();
        
        foreach($tours as $tour){
            $active_suvey = TourSurvey::where('tour_survey.tour_id', '=', $tour->id)
            ->leftJoin('tours', 'tours.id', '=', 'tour_survey.tour_id')
            ->select('tour_survey.id', 'tours.tour_year', 'tours.tour_record', 'tours.tour_id', 'tour_survey.survey_id')
            ->whereNotExists(function($query) use ($uid)
                {
                    $query->select(DB::raw('question_answer.tour_survey_id'))
                          ->from('question_answer')
                          ->whereRaw('tour_survey.id = question_answer.tour_survey_id')
                          ->whereRaw('question_answer.user_id = '.$uid);
                })->get();
            foreach($active_suvey as $as){
                if(!empty($as->tour_id)){
                    $active_surveys[] = $as;
                }
            }
            
        }   
         //echo '<pre>'; print_r($tours); die;     
        return view('trip/home', ['active_surveys'=>$active_surveys, 'attempted_surveys'=>$attempted_surveys, 'user'=>$user]);
    }

    public function feedback(Request $request,$id,$notid=null) {
        
        $uid = Auth::guard('trip')->user()->user_id;
        $user = User::findOrFail($uid);
        $userDetails = $user;
        $tour_survey_id = TourSurvey::findOrFail($id);
        $tour = Tour::findOrFail($tour_survey_id->tour_id);
        $survey = DB::table('surveys')
                ->leftJoin('questionnaires', 'surveys.id', '=', 'questionnaires.survey_id')
                ->where(['questionnaires.survey_id'=>$tour_survey_id->survey_id])
                ->first();
        $notification = $user->notifications()->where('id', $notid)->first();
       //echo '<pre>'; print_r($user); die;
        if ($notification) {
            $notification->markAsRead();
        }            
        if($request->method() == 'POST') {
            //echo '<pre>'; print_r($_POST); die;
            $feedback = Input::get('feedback');
            $comment = Input::get('comment');
            $newarr = ['feedback'=>$feedback, 'comment'=>$comment];
            
           $QuestionAnswer = new QuestionAnswer();
           $QuestionAnswer->answer  =  json_encode($newarr);
           $QuestionAnswer->user_id  =  $uid;
           $QuestionAnswer->tour_survey_id  =  $id;
           if($QuestionAnswer->save()) {
               $emaildata['name'] = $userDetails->name;
               $emaildata['email'] = $userDetails->email;
               $emaildata['feedback'] = $feedback;
               $emaildata['comment'] =  $comment;
               $emaildata['tour_place'] = Input::get('tour_place');
               $emaildata['tour_date'] = Input::get('tour_date');
               $emaildata['tour_driver'] = Input::get('tour_driver');
               $emaildata['tour_bus_no'] = Input::get('tour_bus_no');

                Mail::send('admin.survey.survey_feedback_email', ['survey_feedback' => $emaildata], function($message) use ($userDetails) {
                    $message->to($userDetails->email, $userDetails->name)->subject('Survey Submited Successfully');
                    $message->from('testvirtual8@gmail.com', 'Holiday');
                });
                 
                return redirect('trip/home/')->with('success',"Survey was successfully saved."); 
           }
        }
        return view('trip.survey.questionnaire_feedback',['survey'=>$survey, 'tour'=>$tour, 'user'=>$user]);
    }

    public function surveyListing(Request $request)
    {
        //echo '<pre>'; print_r(Auth::guard('trip')->user()); die;
        $uid = Auth::guard('trip')->user()->user_id;
        $user = User::findOrFail($uid);
        $tours = array();
        $surveys = array();
        $attemptedsurveys = [0];
        $assignedVendors = [0];
        
        $vendors = $user->vendor()->pluck('vendor_id');
        foreach($vendors as $vendorsval){
            $vendor = Vendor::findOrFail($vendorsval);
            $assignedVendors[] = $vendor->vendor_code;
        }
        

        $attempted_surveys = QuestionAnswer::where(['user_id'=>$uid])->select('tour_survey_id')->get();
        $tour = Tour::where('tour_id', Auth::guard('trip')->user()->tour_id)->first();
        
        $active_suvey = TourSurvey::where('tour_survey.tour_id', '=', $tour->id)
        ->leftJoin('tours', 'tours.id', '=', 'tour_survey.tour_id')
        ->select('tour_survey.id', 'tours.tour_year', 'tours.tour_record', 'tours.tour_id', 'tour_survey.survey_id')
        ->whereNotExists(function($query) use ($uid)
            {
                $query->select(DB::raw('question_answer.tour_survey_id'))
                    ->from('question_answer')
                    ->whereRaw('tour_survey.id = question_answer.tour_survey_id')
                    ->whereRaw('question_answer.user_id = '.$uid);
            })->get();
        foreach($active_suvey as $as){
            if(!empty($as->tour_id)){
                $surveys[] = $as;
            }
        }
            
        
       // echo '<pre>'; print_r($surveys); die; 
        //$surveys = Survey::all();
        return view('trip.survey.surveylisting',['surveys'=>$surveys, 'user'=>$user]);
    }

    public function tourListing(Request $request)
	{
        $tour_id = Auth::guard('trip')->user()->tour_id;
        $uid = Auth::guard('trip')->user()->user_id;
        $user = User::findOrFail($uid);
        $tours = Tour::where('tour_id', $tour_id)->get();
		return view('trip.index',['tours'=>$tours, 'user'=>$user]);
	}
}
