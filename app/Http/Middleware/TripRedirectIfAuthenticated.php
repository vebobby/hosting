<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TripRedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //var_dump(Auth::guard($guard)->check()); die;
        if (Auth::guard('trip')->check()) {
            return redirect('/admin/home');
        }

        return $next($request);
    }
}
