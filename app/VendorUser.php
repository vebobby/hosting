<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @package App
 * @property string $title
*/
class VendorUser extends Model
{
    protected $table = 'vendor_user';
    
}
