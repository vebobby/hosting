<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Survey extends Model
{
    use Notifiable;

    public function tour()
    {
        return $this->belongsToMany(tour::class, 'tour_survey');
    }

}


