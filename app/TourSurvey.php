<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourSurvey extends Model
{
	protected $table = 'tour_survey';
}
