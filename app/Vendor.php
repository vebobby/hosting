<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @package App
 * @property string $title
*/
class Vendor extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class, 'vendor_user');
    }
    
}
