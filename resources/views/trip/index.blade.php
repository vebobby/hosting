@inject('request', 'Illuminate\Http\Request')
@extends('layouts.tripapp')

@section('content')
<div class="page-body">
    <!-- Basic table card start -->

@if (Session::has('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('success') }}</strong>
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('error') }}</strong>
</div>
@endif
<div class="card">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="card-header">
    <h5>Tour Listing</h5>
</div>



<div class="card-block table-border-style">
         <div class="table-responsive">
                <table
                    class="table table-bordered table-striped {{ count($tours) > 0 ? 'datatable' : '' }}">
                    <thead>
                        <tr>
                      
                            <th>Tour Year</th>
                            <th>Tour Record #</th>
                            <th>Tour #</th>
                            <th>Where</th>
                            <th>Date</th>
                            <th>Driver</th>
                            <th>Bus #</th>
                            <th>Master Vendor #</th>
                            <th>Sub Vendor #</th>
                            <th>&nbsp;</th>

                        </tr>
                    </thead>

                    <tbody>
                        @if (count($tours) > 0)
                        @foreach ($tours as $tour)
                        <tr data-entry-id="{{ $tour->id }}">
                         

                            <td field-key='name'>{{ $tour->tour_year }}</td>
                            <td field-key='email'>{{ $tour->tour_record }}</td>
                            <td field-key='email'>{{ $tour->tour_id }}</td>
                            <td field-key='email'>{{ $tour->place }}</td>
                            <td field-key='email'>{{ $tour->date }}</td>
                            <td field-key='email'>{{ $tour->driver }}</td>
                            <td field-key='email'>{{ $tour->bus_no }}</td>
                            <td field-key='email'>{{ $tour->master_vendor }}</td>
                            <td field-key='email'>{{ $tour->sub_vendor }}</td>
                            <td>
							<div class="action">
                                
							</div>
                            </td>

                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@stop
