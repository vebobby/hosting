@extends('layouts.tripapp')

@section('content')
<div class="page-body">


    <div class="card">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="card-header">
    <h5>Survey Listing</h5>
</div>

<?php //echo '<pre>'; print_r($surveys); die; ?>
    <!-- Main content -->
    <div class="card-block table-border-style">
         <div class="table-responsive">
        <!-- Main row -->
            <table class="table table-bordered table-striped datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Survey</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            
                            @foreach($surveys as $svs)
                           
                            <tr class="odd gradeX row_{{$svs->id}}">
                                <td>{{$i}}</td>
                                <?php //echo '<pre>'; print_r($user->role->pluck('id')->toArray()[0]); die; ?>
                                @if($user->role->pluck('id')->toArray()[0] == 3)
                                    <td>@if(!empty($svs->tour_id)) Tour Record # {{ $svs->tour_id }} from Tour Year {{ $svs->year }} @else 'NA' @endif</td>
                                @else
                                    <td>{{ $svs->survey_name }}</td> 
                                @endif 
                                <td class="action">
                                  <a href="{{ url('trip/feedback/'.$svs->id) }}" class="feedback_survey"><i class="fas fa-poll"></i></a>
                                </td>

                            </tr>
                            <?php 
								$i++;
							?>
                            @endforeach
                        </tbody>
                    </table>
             
     
        <!-- /.row (main row) -->
        </div>
    </div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $('body').on('click', '.delete_ques', function() {
        var subid = $(this).attr('id');
        var r = confirm("Do you want to delete?");
        if (r == true) {
            $.ajax({
                type: "GET",
                url: "{{ url('admin/deletesurvey') }}" + '/' + subid,
                success: function(response) {
                    if (response == 'success') {
                        //location.reload();

                        $('.row_'+subid).css('display', 'none');
                    }
                }
            });
        }
    });
});
</script>
@endsection