<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Survey Feedback</title>
	<style>
/* -------------------------------------
INLINED WITH htmlemail.io/inline
------------------------------------- */
/* -------------------------------------
RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 620px) {
	table[class=body] h1 {
		font-size: 28px !important;
		margin-bottom: 10px !important;
	}
	table[class=body] p,
	table[class=body] ul,
	table[class=body] ol,
	table[class=body] td,
	table[class=body] span,
	table[class=body] a {
		font-size: 16px !important;
	}
	table[class=body] .wrapper,
	table[class=body] .article {
		padding: 10px !important;
	}
	table[class=body] .content {
		padding: 0 !important;
	}
	table[class=body] .container {
		padding: 0 !important;
		width: 100% !important;
	}
	table[class=body] .main {
		border-left-width: 0 !important;
		border-radius: 0 !important;
		border-right-width: 0 !important;
	}
	table[class=body] .btn table {
		width: 100% !important;
	}
	table[class=body] .btn a {
		width: 100% !important;
	}
	table[class=body] .img-responsive {
		height: auto !important;
		max-width: 100% !important;
		width: auto !important;
	}
}

/* -------------------------------------
PRESERVE THESE STYLES IN THE HEAD
------------------------------------- */
@media all {
	.ExternalClass {
		width: 100%;
	}
	.ExternalClass,
	.ExternalClass p,
	.ExternalClass span,
	.ExternalClass font,
	.ExternalClass td,
	.ExternalClass div {
		line-height: 100%;
	}
	.apple-link a {
		color: inherit !important;
		font-family: inherit !important;
		font-size: inherit !important;
		font-weight: inherit !important;
		line-height: inherit !important;
		text-decoration: none !important;
	}
	#MessageViewBody a {
		color: inherit;
		text-decoration: none;
		font-size: inherit;
		font-family: inherit;
		font-weight: inherit;
		line-height: inherit;
	}
	.btn-primary table td:hover {
		background-color: #34495e !important;
	}
	.btn-primary a:hover {
		background-color: #34495e !important;
		border-color: #34495e !important;
	}
}
</style>
</head>
<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
	<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
		<tr>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
			<td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
				<div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">
					<table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
					<?php //echo '<pre>'; var_dump($survey_feedback['tour']); die; ?>
						<!-- START MAIN CONTENT AREA -->
						<tr>
							<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
								<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
									<tr>
										<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
										
											<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi {{ $survey_feedback['name'] }},</p>
			<p>Just a note to personally thank you for choosing Holiday for your trip to «{{$survey_feedback['tour_place']}}» on «{{$survey_feedback['tour_date']}}».  Your driver was «{{$survey_feedback['tour_driver']}}» on Coach number «{{$survey_feedback['tour_bus_no']}}».</p>
			<p>Here at Holiday, we are always striving to provide the best, most professional service in the tour industry.  Because you, our valued customer, are so important to us, we would like to ask you a few questions to continue our quality tours.</p>

											<h3 style="font-family: sans-serif; font-size: 16px; font-weight: normal; margin: 0; Margin-bottom: 15px;text-align:center">Survey Feedback.</h3>
											<?php foreach($survey_feedback['feedback'] as $val){
												 //echo '<pre>'; print_r($survey_feedback); die;
												echo '<p style="font-family: sans-serif; font-size: 12px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Q) '.$val["'question'"].'</p>';
												if($val["'type_of_question'"] == 'multiple_question' || $val["'type_of_question'"] == 'subqsn'){
													foreach($val["'answer'"] as $k => $sval){
														if($val["'type_of_question'"] == 'subqsn'){
															echo '<p style="font-family: sans-serif; font-size: 12px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Ans) '.$val["'sub_questions'"][$k].': '.$sval[0].'</p>';
														}else{
														//echo '<pre>'; print_r($val); die;
														echo '<p style="font-family: sans-serif; font-size: 12px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Ans) '.$sval[0].'</p>';
														}
													}
												}else{
													echo '<p style="font-family: sans-serif; font-size: 12px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Ans) '.$val["'answer'"].'</p>';
												}
												
											} ?>
											<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Comment:-</p>
											<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> <?php echo $survey_feedback["comment"]; ?></p>
											
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<!-- END MAIN CONTENT AREA -->
					</table>
					<!-- END CENTERED WHITE CONTAINER -->
				</div>
			</td>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		</tr>
	</table>
</body>
</html>