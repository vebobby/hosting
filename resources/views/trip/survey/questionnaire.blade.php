@extends('layouts.app')

@section('content')
<div class="page-body">
    <div class="row">
<!-- Right side column. Contains the navbar and content of the page -->
<h3 class="page-title">Surveys</h3>
@if (Session::has('message'))
<div class="alert alert-info">
	<p>{{ Session::get('message') }}</p>
</div>
@endif
@if ($errors->count() > 0)
<div class="alert alert-danger">
	<ul class="list-unstyled">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@can('survey_create')
    <p>
        <a href="{{ url('admin/questionnaires/create') }}" class="btn btn-success">Add</a>
        
    </p>
@endcan

<div class="card" style="width:100%">
    <div class="col-sm-12">
        <div class="panel-heading"></div>
    <!-- Main content -->
    <div class="panel-body table-responsive">
        
        <!-- Main row -->
     
                <div class="panel-body table-responsive">
                    @if (Session::has('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{Session::get('success') }}</strong>
                    </div>
                    @endif
                    @if (Session::has('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{Session::get('error') }}</strong>
                    </div>
                    @endif
                   
            <table class="table table-bordered table-striped datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Question</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            @foreach($questionnaires as $ques)

                            <tr class="odd gradeX row_{{$ques->id}}">
                                <td>{{$i}}</td>
                                <td>{{ !empty($ques->question)?$ques->question:'NA' }}</td>
                                <td class="action">
                                @can('survey_feedback') <a href="{{ url('admin/feedback/'.$ques->id) }}" class="feedback_survey"><i class="fas fa-poll"></i></a> @endcan
                                @can('survey_edit') <a href="{{ url('admin/questionnaires/edit/'.$ques->id) }}" class="edit_sub"><i class="ti-pencil"></i></a> @endcan
                                @can('survey_delete') <a href="javascript:void(0);" id="{{$ques->id}}" class="delete_ques"><i class="ti-trash"></i></a> @endcan
                                </td>

                            </tr>
                            <?php 
									$i++;
								?>
                            @endforeach


                        </tbody>
                    </table>
             
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.content -->
</div>
</div>
</div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $('body').on('click', '.delete_ques', function() {
        var subid = $(this).attr('id');
        var r = confirm("Do you want to delete question?");
        if (r == true) {
            $.ajax({
                type: "GET",
                url: "{{ url('admin/deletequestionnaire') }}" + '/' + subid,
                success: function(response) {
                    if (response == 'success') {
                        //location.reload();
                        $('.row_'+subid).css('display', 'none');
                    }
                }
            });
        }
    });
});
</script>
@endsection