@extends('layouts.app')

@section('content')
<h3 class="page-title">@lang('global.app_change_password')</h3>
<div class="panel-heading">
</div>
<div class="col-sm-12">
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger" style="width:100%">
        <strong>@lang('global.app_whoops')</strong> @lang('global.app_there_were_problems_with_input'):
        <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div> 
@endif
    @if(session('success'))
    <!-- If password successfully show message -->
    <div class="row">
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    </div>
    @else
    {!! Form::open(['method' => 'PATCH', 'route' => ['auth.change_password']]) !!}
    <!-- If no success message in flash session show change password form  -->

    <div class="panel-body">

        <div class="row">
            <div class="card" style="width:100%">
                <div class="card-block">
                    @if (!auth()->user()->invitation_token)
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">
                            {!! Form::label('current_password', trans('global.app_current_password'), ['class' =>
                            'control-label']) !!}
                        </label>
                        <div class="col-sm-10">
                            {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => '']) !!}
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('current_password'))
                        <p class="help-block">
                            {{ $errors->first('current_password') }}
                        </p>
                        @endif
                    </div>

                    @endif
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">
                            {!! Form::label('new_password', trans('global.app_new_password'), ['class' =>
                            'control-label'])
                            !!}
                        </label>
                        <div class="col-sm-10">
                            {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => '']) !!}
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">
                            {!! Form::label('new_password_confirmation', trans('global.app_password_confirm'),
                            ['class'
                            =>
                            'control-label']) !!}
                        </label>
                        <div class="col-sm-10">
                            {!! Form::password('new_password_confirmation', ['class' => 'form-control',
                            'placeholder' =>
                            ''])
                            !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
    @endif
</div>
@stop