@extends('layouts.auth')

@section('content')
    <div class="row">
	  <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
         <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container">
        <div class="row">
            <div class="col-sm-12">
                 <div class="login-card card-block auth-body mr-auto ml-auto">
					<div class="auth-box">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}
							<div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-left txt-primary">Sign Up</h3>
                                    </div>
                                </div>
                                <hr/>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="name" placeholder="Enter your name" value="{{ old('name') }}" autofocus>

                                    @if ($errors->has('name'))
                                    <span class="help-block" style="display:table">
                                        <strong class="alert-danger">{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" placeholder="Enter your email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block" style="display:table">
                                        <strong class="alert-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                
                                <div class="col-md-12">
                                    <input id="password" type="password" placeholder="Enter password" class="form-control" name="password" >

                                    @if ($errors->has('password'))
                                    <span class="help-block" style="display:table">
                                        <strong class="alert-danger">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                
                                <div class="col-md-12">
                                    <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                        @lang('global.app_register')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		</div>
	</section>
    </div>
@endsection