@extends('layouts.auth')

@section('content')
    <div class="row">
	  <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
         <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <div class="login-card card-block auth-body mr-auto ml-auto">
                    
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>@lang('global.app_whoops')</strong> @lang('global.app_there_were_problems_with_input'):
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
					<div class="auth-box">
                    <form class="form-horizontal"
                          role="form"
                          method="POST"
                          action="{{ url('login') }}">
						  
						  <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-left txt-primary">Sign In</h3>
                                    </div>
                                </div>
                                <hr/>
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">
						<div class="input-group">
							<input type="email"
                                       class="form-control"
                                       name="email"
                                       value="{{ old('email') }}" placeholder="Your Email Address">
							<span class="md-line"></span>
						</div>
                       <div class="input-group">
							 <input type="password"
                                       class="form-control"
                                       name="password" placeholder="Password">
							<span class="md-line"></span>
						</div>

                        

						<div class="row m-t-25 text-left">
                                    <div class="col-sm-7 col-xs-12">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label>
                                               <input type="checkbox"
                                           name="remember"> 
                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                <span class="text-inverse">@lang('global.app_remember_me')</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-xs-12 forgot-phone text-right">
                                        <a href="{{ route('auth.password.reset') }}">@lang('global.app_forgot_password')</a>
                                <br>
                                 <a href="{{ route('auth.register') }}">@lang('global.app_registration')</a>
                                
                                    </div>
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit"
                                        class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                    @lang('global.app_login')
                                </button>
                                    </div>
                                </div>
                                <hr/>
					 	
                    </form>
					</div>
                  </div>
                    <!-- Authentication card end -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
    </div>
@endsection