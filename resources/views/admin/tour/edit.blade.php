@extends('layouts.app')

@section('content')
<div class="page-body">
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Edit User</h5>
                </div>
                <div class="card-block">
                {!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id]]) !!}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">{!! Form::label('name',
                                    trans('global.users.fields.name').'*',
                                    ['class' => 'control-label']) !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '',
                                    'required'
                                    => ''])
                                    !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                                @endif
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">{!! Form::label('email',
                                    trans('global.users.fields.email').'*', ['class' =>
                                    'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' =>
                                    '',
                                    'required' =>
                                    '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('email'))
                                <p class="help-block">
                                    {{ $errors->first('email') }}
                                </p>
                                @endif
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">{!! Form::label('password',
                                    trans('global.users.fields.password').'*', ['class' =>
                                    'control-label']) !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('password'))
                                <p class="help-block">
                                    {{ $errors->first('password') }}
                                </p>
                                @endif
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> {!! Form::label('role',
                                    trans('global.users.fields.role').'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::select('role[]', $roles, old('role') ? old('role') :
                                    $user->role->pluck('id')->toArray(),
                                    ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('role'))
                                <p class="help-block">
                                    {{ $errors->first('role') }}
                                </p>
                                @endif
                            </div>
                            <?php if($user->role->pluck('title')->toArray()[0] == 'Customer'){ ?>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> {!! Form::label('master_vendor',
                                    'Master Vendors'.'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::select('master_vendors[]', $master_vendors, old('master_vendors') ? old('master_vendors') :
                                   $user->vendor->pluck('id')->toArray(),
                                    ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                </div>
                           </div> 

                           <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> {!! Form::label('sub_vendor',
                                    'Sub Vendors'.'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::select('sub_vendors[]', $sub_vendors, old('sub_vendors') ? old('sub_vendors') :
                                    $user->vendor->pluck('id')->toArray(),
                                    ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                </div>
                           </div> 
                          <?php } ?>

                      
            {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
            </div>
    </div>
</div>
</div>
</div>
@stop