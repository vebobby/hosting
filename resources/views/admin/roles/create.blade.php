@extends('layouts.app')

@section('content')
<div class="page-body">
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Create Role</h5>
                </div>
                <div class="card-block">
    {!! Form::open(['method' => 'POST', 'route' => ['admin.roles.store']]) !!}

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label"> {!! Form::label('title',
                            trans('global.roles.fields.title').'*', ['class' => 'control-label'])
                            !!}</label>
                        <div class="col-sm-10">
                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '',
                            'required' =>
                            '']) !!}
                        </div>



                        <p class="help-block"></p>
                        @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                        @endif
                    </div>

                    <div class="form-group row">

                        <label class="col-sm-2 col-form-label"> {!! Form::label('permission',
                            trans('global.roles.fields.permission').'*', ['class' =>
                            'control-label']) !!}</label>
                        <div class="col-sm-10">
                            {!! Form::select('permission[]', $permissions, old('permission'), ['class' => 'form-control
                            select2', 'multiple' => 'multiple', 'required' => '']) !!}
                        </div>

                        <p class="help-block"></p>
                        @if($errors->has('permission'))
                        <p class="help-block">
                            {{ $errors->first('permission') }}
                        </p>
                        @endif
                    </div>
    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
    </div>
    </div>
</div>
</div>
</div>
@stop