@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<div class="page-body">
    <!-- Basic table card start -->
    @can('role_create')
    <p>
        <a class="btn btn-success" href="{{ route('admin.roles.create') }}" class="">Create</a>
        
    </p>
    @endcan
@if (Session::has('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('success') }}</strong>
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('error') }}</strong>
</div>
@endif
<div class="card">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="card-header">
    <h5>Role Listing</h5>
</div>
<div class="card-block table-border-style">
         <div class="table-responsive">
            <table class="table table-bordered table-striped {{ count($roles) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                       

                        <th>@lang('global.roles.fields.title')</th>
                        <!--<th>@lang('global.roles.fields.permission')</th>-->
                                                <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($roles) > 0)
                        @foreach ($roles as $role)
                            <tr data-entry-id="{{ $role->id }}">
                              

                                <td field-key='title'>{{ $role->title }}</td>
                               <!-- <td field-key='permission'>
                                    @foreach ($role->permission as $singlePermission)
                                        <span class="label label-info label-many">{{ $singlePermission->title }}</span>
                                    @endforeach
                                </td>-->
                                                                <td class="action">
                                    @can('role_view')
                                    <a href="{{ route('admin.roles.show',[$role->id]) }}" class="bob_no_btn"><i class="ti-view-list"></i></a>
                                    @endcan
                                    @can('role_edit')
                                    <a href="{{ route('admin.roles.edit',[$role->id]) }}" class="bob_no_btn"><i class="ti-pencil"></i></a>
                                    @endcan
                                    @can('role_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.roles.destroy', $role->id])) !!}
                                    {!! Form::button('<i class="ti-trash"></i>', array('type' => 'submit','class' => 'bob_no_btn')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript') 
    <script>
        @can('role_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.roles.mass_destroy') }}';
        @endcan

    </script>
@endsection