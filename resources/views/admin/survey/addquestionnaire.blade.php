@extends('layouts.app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<div class="page-body">
    <div class="row">
        <!-- Content Header (Page header) -->
        <h3 class="page-title">Survey</h3>
        <div class="col-sm-12">
        <!-- Main content -->
        <div class="panel-heading"></div>              
            <form method="post" id="add-questionnaire" autocomplete="off">
                <div class="panel-body">
                    <div class="row">
                        <div class="card" style="width:100%">
                            <div class="card-block">
                                <input type="hidden" class="form-control" name="survey_id" value="{{ !empty($s_id)?$s_id:'' }}">
                               <!-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"> Question: <span
                                    class="mandatory_field">*</span> </label>
                                    <div class="col-sm-10">
                                        {!! Form::textarea('question', old('question'), ['class' => 'form-control', 'placeholder' => '',
                                        'required'
                                        => ''])
                                        !!}
                                    </div>
                                </div>-->
                                <div class="form-group1 row qsnrow">
                                    <label class="col-sm-2 col-form-label"> Question): <span
                                    class="mandatory_field">*</span> </label>
                                    <div class="col-sm-9">
                                        <!-- {!! Form::text('qsn[]', old('qsn[]'), ['class' => 'form-control', 'placeholder' => '',
                                        'required'
                                        => ''])
                                        !!} -->
                                        
                                        <div class="form-group row">
                                        <div class="col-sm-12">
                                        <input class="form-control qsninput" type="text" name="qsn[]" index="0">
                                        <a href="javascript:void(0);" style="float: right;padding: 5px;text-decoration: underline;" class="add-sub-qsn">Add Sub Question</a>
                                            <div class="col-sm-12 sub-qsn-section"></div>
                                        </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div style="margin-top: 7px;"><i title="Add Answer" id="add-subqsn"
                                        class="fa fa-plus-circle" aria-hidden="true"></i></div>
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <div id="answer-section" class="col-sm-12"></div>
                                </div>
                               {!! csrf_field() !!}
                                <br>
                                <button type="submit" class="btn btn-primary" id="btn_save_school">Submit</button>
                            </div>
                        <!-- /.row (main row) -->
                        </div><!-- /.content -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $("#add-questionnaire").validate({
        rules: {
            question: "required",
            'answer[]': "required"
        },
        messages: {
            question: "Please fill the question.",
            'answer[]': "Please fill the answer."
        }
    });
    $('#add-subqsn').click(function() {
        var qsnlength = $('.qsnrow').length;
        if ($('.qsnrow').length < 5) {
            var answerHtml =
                '<div style="margin-top:7px;" class="qsnrow row "><label class="col-sm-2 col-form-label">Question)</label><div class="col-sm-9" ><input class="form-control qsninput" index="'+qsnlength+'" type="text" name="qsn[]"><div class="form-group row"><div class="col-sm-12"><a href="javascript:void(0);" style="float: right;padding: 5px;text-decoration: underline;" class="add-sub-qsn">Add Sub Question</a><div class="sub-qsn-section" class="col-sm-12"></div></div></div></div><div class="col-lg-1"><div style="margin-top: 7px;"><i class="remove-answer fa fa-times" aria-hidden="true"></i></div></div></div>';
            $('#answer-section').append(answerHtml);
        }
    });

    var i = 1;
    $(document).on('click', '.add-sub-qsn', function() {
      // var index = $(this).prev('.qsninput').attr('index');
      console.log($(this).closest('.qsninput'));
      var index = $(this).closest('.qsnrow').find('.qsninput').attr('index');
        if ($(this).next('.subqsnrow').length < 5) {
            var subqsnHtml =
                '<div style="margin-top:40px;" class="subqsnrow row "><label class="col-sm-2 col-form-label">Sub Qsn)</label><div class="col-sm-9" ><input class="form-control" type="text" name="subqsn['+index+'][]"></div><div class="col-lg-1"><div style="margin-top: 7px;"><i class="remove-answer fa fa-times" aria-hidden="true"></i></div></div></div>';
            $(this).next('.sub-qsn-section').append(subqsnHtml);
            i = i+1;
        }
    });

    $("body").on("click", ".remove-answer", function() {
        $(this).parent().parent().parent().remove();
    });

});
</script>
@endsection