@extends('layouts.app')
@section('content')
<style>
.side_bar [type="radio"]:checked, [type="radio"]:not(:checked) {
    position: unset;
    left: -9999px;
}
</style>
<div class="page-body">
<div class="row">
	<div class="col-sm-12">
		<!-- Basic Form Inputs card start -->
		<div class="card">
		<div class="form-group">
		<p></p>
		<div class="col-sm-12">
			<p>Dear «{{Auth::user()->name}}»,</p>
			<p>Just a note to personally thank you for choosing Holiday for your trip to «{{$tour->place}}» on «{{$tour->date}}».  Your driver was «{{$tour->driver}}» on Coach number «{{$tour->bus_no}}».</p>
			<p>Here at Holiday, we are always striving to provide the best, most professional service in the tour industry.  Because you, our valued customer, are so important to us, we would like to ask you a few questions to continue our quality tours.</p>
			</div>
			</div>
			<div class="card-header">
				<h5>Questions</h5>
			</div>
				<div class="card-block">
		@if(count($survey) > 0)
		<?php
		$alphaarr = array(0=>'a', 1=>'b', 2=>'c', 3=>'d', 4=>'e');
		if(!empty($survey->question)){
			$question = json_decode($survey->question);
		}else{
			$question = array();
		}

		if(!empty($question_answer->answer)){
			$answer = json_decode($question_answer->answer);
		}else{
			$answer = array();
		}
		//echo '<pre>'; print_r($question); die;
		?>
		<form method="post" id="questionnaire" autocomplete="off">
			<div class="form-group">
			@php $counter=1; $i=0 @endphp
			
			@if(count($question)>0)		
			@foreach($question as $qk => $ques)
			<?php $qsntype = $ques->question_type; ?>
			@if($qsntype != 'regular')	
				<div class="form-group row">
						<label class="bob col-sm-8 col-form-label">
							Q{{ $counter.') '.$ques->question }}
						</label>
						<input type="hidden" name="feedback[{{$i}}]['question']" value="{{$ques->question}}" />
				</div>
			@endif		
				<?php 
					if(!empty($ques->subqsn)){
						$sub_question = $ques->subqsn;
					}else{
						$sub_question = array();
					}
				?>
				
				@if($qsntype == 'regular')	
				<input type="hidden" name="feedback[{{$i}}]['question']" value="{{$ques->question}}" />
				<div class="form-group row">	
						<label class="bob col-sm-2 col-form-label">
						</label>
						<div class="col-sm-10">
						<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
							<label class="feedback_checkbox">Excellent</label>
							<label class="feedback_checkbox">Good</label>
							<label class="feedback_checkbox">Fair</label>
							<label class="feedback_checkbox">Poor</label>
						</div>
						</div>
						</div>	
					</div>	
					<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 bob">Q{{ $counter.') '}}</legend>
						<div class="col-sm-10">
						
						<div class="row">
						<div class="col-sm-12">
						<div class="row">
						<label class="bob col-sm-6 col-form-label">
						{{$ques->question}}
					</label>
				
					<input type="hidden" name="feedback[{{$i}}]['sub_questions'][]" value="{{$ques->question}}" />
					<input type="hidden" name="feedback[{{$i}}]['type_of_question']" value="{{$qsntype}}" />
					<div class="col-sm-6 question">
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" data-error="#errNm{{$i}}" required  name="feedback[{{$i}}]['answer'][]" value="excellent" /></label>
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" required  name="feedback[{{$i}}]['answer'][]" value="good" /></label>
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" required  name="feedback[{{$i}}]['answer'][]" value="fair" /></label>
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" required  name="feedback[{{$i}}]['answer'][]" value="poor" /></label>
						<span id="errNm{{$i}}"></span>
					</div>
					</div>
					</div>
					</div>
						
						</div>
					</div>
					
					</fieldset>

				@endif
				
					@if($qsntype == 'subqsn')	
					
					@if(count($sub_question)>0)	
					<div class="form-group row">	
						<label class="bob col-sm-2 col-form-label">
						</label>
						<div class="col-sm-10">
						<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
							<label class="feedback_checkbox">Excellent</label>
							<label class="feedback_checkbox">Good</label>
							<label class="feedback_checkbox">Fair</label>
							<label class="feedback_checkbox">Poor</label>
						</div>
						</div>
						</div>	
					</div>	
					<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Answers</legend>
						<div class="col-sm-10">
						@foreach($sub_question as $sk => $sqsn)
						<div class="row">
						<div class="col-sm-12">
						<div class="row">
						<label class="bob col-sm-6 col-form-label">
						{{$sqsn}}
					</label>
				
					<input type="hidden" name="feedback[{{$i}}]['sub_questions'][]" value="{{$sqsn}}" />
					<input type="hidden" name="feedback[{{$i}}]['type_of_question']" value="{{$qsntype}}" />
					<div class="col-sm-6 question">
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" data-error="#errNm{{$i}}{{$sk}}" required <?php if(isset($answer[$qk]->sub_question_review[$sk])){if($answer[$qk]->sub_question_review[$sk] == 'excellent'){ echo 'checked';} echo ' disabled';} ?> name="feedback[{{$i}}]['answer'][{{$sk}}][]" value="excellent" /></label>
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" required <?php if(isset($answer[$qk]->sub_question_review[$sk])){if($answer[$qk]->sub_question_review[$sk] == 'good'){ echo 'checked';} echo ' disabled';} ?> name="feedback[{{$i}}]['answer'][{{$sk}}][]" value="good" /></label>
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" required <?php if(isset($answer[$qk]->sub_question_review[$sk])){if($answer[$qk]->sub_question_review[$sk] == 'fair'){ echo 'checked';} echo ' disabled';} ?> name="feedback[{{$i}}]['answer'][{{$sk}}][]" value="fair" /></label>
						<label class="col-sm-1 feedback_checkbox"><input type="checkbox" required <?php if(isset($answer[$qk]->sub_question_review[$sk])){if($answer[$qk]->sub_question_review[$sk] == 'poor'){ echo 'checked';} echo ' disabled';} ?> name="feedback[{{$i}}]['answer'][{{$sk}}][]" value="poor" /></label>
						<span id="errNm{{$i}}{{$sk}}"></span>
						<sectin class="bob"></section>
					</div>
					</div>
					</div>
					</div>
						@endforeach
						</div>
					</div>
					
					</fieldset>
					@endif
					
					@endif
					@if($qsntype == 'multiple_question')
					<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Answers</legend>
						<div class="col-sm-10">
							@if(count($sub_question)>0)	
								@foreach($sub_question as $sk => $sqsn)
								<input type="hidden" name="feedback[{{$i}}]['sub_questions'][]" value="{{$sqsn}}" />
								<input type="hidden" name="feedback[{{$i}}]['type_of_question']" value="{{$qsntype}}" />
								<div class="form-check">
									<input class="form-check-input" type="radio" data-error="#errNmb{{$i}}"  required <?php if(isset($answer[$qk]->sub_question_review[$sk])){if($answer[$qk]->sub_question_review[$sk] == 'excellent'){ echo 'checked';} echo ' disabled';} ?> name="feedback[{{$i}}]['answer'][]" value="{{$sqsn}}">
									<label class="form-check-label" for="gridRadios1">
									{{$sqsn}}
									</label>
								</div>
							@endforeach
							
							@endif
							<span id="errNmb{{$i}}"></span>
						</div>
						
					</div>
					
					</fieldset>
					@endif

					@if($qsntype == 'single_input')
					<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Answers</legend>
						<div class="col-sm-10">	
					@if(count($sub_question)>0)	
						@foreach($sub_question as $sk => $sqsn)
					<input type="hidden" name="feedback[{{$i}}]['sub_questions'][]" value="{{$sqsn}}" />
					<input type="hidden" name="feedback[{{$i}}]['type_of_question']" value="{{$qsntype}}" />
					
					<div class="form-check">
						<input class="form-control" type="text"  required  name="feedback[{{$i}}]['answer']" value="{{$sqsn}}" >
					</div>
					@endforeach
					@endif
					</div>
					</div>
					</fieldset>
					
					@endif

					@if($qsntype == 'textarea_input')
					<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Answers</legend>
						<div class="col-sm-10">		
					@if(count($sub_question)>0)	
						@foreach($sub_question as $sk => $sqsn)
					<input type="hidden" name="feedback[{{$i}}]['sub_questions'][]" value="{{$sqsn}}" />
					<input type="hidden" name="feedback[{{$i}}]['type_of_question']" value="{{$qsntype}}" />
					
					<div class="form-check">
					<textarea class="form-control" required  name="feedback[{{$i}}]['answer']" value="{{$sqsn}}" ></textarea>
					</div>
					@endforeach
					@endif
					</div>
					</div>
					</fieldset>
					@endif

					@if($qsntype == 'fill_in_blank')
					<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Answers</legend>
						<div class="col-sm-10">		
					@if(count($sub_question)>0)	
						@foreach($sub_question as $sk => $sqsn)
					<input type="hidden" name="feedback[{{$i}}]['sub_questions'][]" value="{{$sqsn}}" />
					<input type="hidden" name="feedback[{{$i}}]['type_of_question']" value="{{$qsntype}}" />
					<div class="form-check">
					<textarea class="form-control" required  name="feedback[{{$i}}]['answer']" >{{$sqsn}}</textarea>
					</div>
					@endforeach
					@endif
					</div>
					</div>
					</fieldset>
					@endif
					
				@php $counter++; $i++; @endphp
			@endforeach
			<hr />
				<fieldset class="form-group">	
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Comment</legend>
						<div class="col-sm-10">	
						<div class="form-check">
							<textarea rows="10" class="form-control"  name="comment" ></textarea>
						</div>
						</div>
					</div>
				</fieldset>	
			@endif
			{!! csrf_field() !!}
			@if(count($answer)==0)
				<button type="submit" class="btn btn-primary" id="btn_save_questionarrie">Submit</button>
			@endif
			</div>
			<input type="hidden" value="{{$tour->place}}" name="tour_place" />
			<input type="hidden" value="{{$tour->date}}" name="tour_date" />
			<input type="hidden" value="{{$tour->driver}}" name="tour_driver" />
			<input type="hidden" value="{{$tour->bus_no}}" name="tour_bus_no" />
		</form>
		@endif
		
		</div>
		</div>
    </div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$.extend(jQuery.validator.messages, {
		    required: "This field is required."
		});

		
		// $("#questionnaire").validate({
		// 	errorPlacement: function (error, element) {
		// 	console.log($(element).parents('div'));
		// 		error.insertAfter($(element).parents('div.col-sm-4.question').children('.bob'));
				
		// 	},

		
		// });

		$("#questionnaire").validate({
        
        errorPlacement: function(error, element) {
        var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
		$('.question').each(function(){
			var qid=$(this).attr('qid');
			$("[name='answer["+qid+"]']").rules("add", "required");;
		});

		$("input:checkbox").on('click', function() {
			// in the handler, 'this' refers to the box clicked on
			var $box = $(this);
			if ($box.is(":checked")) {
				// the name of the box is retrieved using the .attr() method
				// as it is assumed and expected to be immutable
				var group = "input:checkbox[name='" + $box.attr("name") + "']";
				// the checked state of the group/box on the other hand will change
				// and the current value is retrieved using .prop() method
				$(group).prop("checked", false);
				$box.prop("checked", true);
			} else {
				$box.prop("checked", false);
			}
		});

		// $('#questionnaire').submit(function(){
			
		// 	$("input:checkbox").each(function() {
		// 	// in the handler, 'this' refers to the box clicked on
		// 	var $box = $(this);
			
		// 		// the name of the box is retrieved using the .attr() method
		// 		// as it is assumed and expected to be immutable
		// 		var group = "input:checkbox[name='" + $box.attr("name") + "']:checked";
		// 		// the checked state of the group/box on the other hand will change
		// 		// and the current value is retrieved using .prop() method
		// 		var count_length = $(group).length;
				
		// 		if(count_length == 0){
		// 			$('#btn_save_questionarrie').addClass('disabled');
		// 			return false;
		// 		}
			
		// });
		// });

			
		
	});


</script>
@endsection