@extends('layouts.app')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<div class="page-body">
<div class="row">
                                            <div class="col-sm-12">
                                                <!-- Basic Form Inputs card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Edit Question</h5>
                                                        </div>
                                                        <div class="card-block">

 
            <form method="post" id="add-questionnaire" autocomplete="off">
                <input type="hidden" class="form-control" name="survey_id" value="{{ !empty($s_id)?$s_id:'' }}">
                <div class="form-group row">
                <label class="col-sm-2 col-form-label"> Question: <span
                            class="mandatory_field">*</span> </label>
                    <div class="col-sm-10">
                    <textarea class="form-control" name="question">{{ $questionnaireData->question }}</textarea>
                        </div>
                    
                    
                </div>
                @php $answers = json_decode($questionnaireData->answer); @endphp
                <div class="row anserrow form-group">
                  
                <label class="col-sm-2 col-form-label"> Answer: <span
                                    class="mandatory_field">*</span> </label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="{{ !empty($answers[0])?$answers[0]:'' }}"
                                name="answer[]">
                                </div>
                           

                       
                  
                                <div class="col-sm-1">
                        <div style="margin-top: 7px;"><i title="Add Answer" id="add-answer"
                                class="fa fa-plus-circle" aria-hidden="true"></i></div>
                    </div>   

                </div>
                <div class="form-group row">
                <div id="answer-section" class="col-sm-12">
                    @php unset($answers[0]); @endphp
                    @foreach($answers as $ans)
                    @if(!empty($ans))
                    <div style="margin-top:7px;" class="anserrow row">
                    <label class="col-sm-2 col-form-label"> </label>
                        <div class="col-lg-9">
                            <input value="{{ $ans }}" class="form-control" type="text" name="answer[]">
                        </div>
                        <div class="col-lg-1">
                            <div><i class="remove-answer fa fa-times" aria-hidden="true"></i></div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                </div>
                {!! csrf_field() !!}
                <br>
                <button type="submit" class="btn btn-primary" id="btn_save_school">Submit</button>
            </form>
            </div>
    </div>
</div>
</div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $("#add-questionnaire").validate({
        rules: {
            question: "required",
            'answer[]': "required"
        },
        messages: {
            question: "Please enter the question.",
            'answer[]': "Please enter your answer."
        }
    });
    $('#add-answer').click(function() {
        if ($('.anserrow').length < 5) {
            var answerHtml =
                '<div style="margin-top:7px;" class="anserrow row "><label class="col-sm-2 col-form-label"> </label><div class="col-sm-9" ><input class="form-control" type="text" name="answer[]"></div><div class="col-lg-1"><div style="margin-top: 7px;"><i class="remove-answer fa fa-times" aria-hidden="true"></i></div></div></div>';
            $('#answer-section').append(answerHtml);
        }
    });
    $("body").on("click", ".remove-answer", function() {
        $(this).parent().parent().parent().remove();
    });

});
</script>
@endsection