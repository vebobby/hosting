@extends('layouts.app')

@section('content')
<div class="page-body">
    <!-- Basic table card start -->
    @can('survey_create')
    <p>
        <a href="{{ url('admin/questionnaires/create/'.$s_id) }}" class="btn btn-success">Add Question</a>
        
    </p>
@endcan
@if (Session::has('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('success') }}</strong>
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('error') }}</strong>
</div>
@endif
    <div class="card">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="card-header">
    <h5>Question Listing</h5>
</div>


    <!-- Main content -->
    <div class="card-block table-border-style">
         <div class="table-responsive">
        <!-- Main row -->
            <table class="table table-bordered table-striped datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Question</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($questions as $qsns)
                            <?php
                                $question = json_decode($qsns->question);
                            ?>
                            <?php $i=1;?>
                            @foreach($question as $qsn)
                            
                            <tr class="odd gradeX row_{{$qsns->id}}">
                                <td>{{$i}}</td>
                                <td>{{ !empty($qsn->question)?$qsn->question:'NA' }}</td>
                                <td class="action">
                                 <a href="{{ url('admin/questionnaires/edit/'.$qsns->id) }}" class="edit_sub"><i class="ti-pencil"></i></a> 
                                 <a href="javascript:void(0);" id="{{$qsns->id}}" class="delete_ques"><i class="ti-trash"></i></a> 
                            </td>

                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endforeach


                        </tbody>
                    </table>
             
     
        <!-- /.row (main row) -->
        </div>
    </div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $('body').on('click', '.delete_ques', function() {
        var subid = $(this).attr('id');
        var r = confirm("Do you want to delete question?");
        if (r == true) {
            $.ajax({
                type: "GET",
                url: "{{ url('admin/deletequestionnaire') }}" + '/' + subid,
                success: function(response) {
                    if (response == 'success') {
                        //location.reload();
                        $('.row_'+subid).css('display', 'none');
                    }
                }
            });
        }
    });
});
</script>
@endsection