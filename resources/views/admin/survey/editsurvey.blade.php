@extends('layouts.app')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<div class="page-body">
<div class="row">
        <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Edit Survey</h5>
                    </div>
                    <div class="card-block">
                    <?php
                    if(!empty($survey->question)){
                        $question = json_decode($survey->question);
                    }else{
                        $question = array();
                    }
                       //  echo '<pre>'; print_r($question); die;
                    ?>
                    <form method="post" id="add-questionnaire" autocomplete="off">
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label"> Survey: <span class="mandatory_field">*</span> </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="survey_name" value="{{ $survey->survey_name }}" />
                            </div> 
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> {!! Form::label('tour',
                                    'Select Tour'.'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::select('tour[]', $tours, old('tour') ? old('tour') :
                                    $getSurvey->tour->pluck('id')->toArray(),
                                    ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                </div>
                           </div> 
                        <?php $i = 0; ?>
                        @if(count($question)>0)
                        @foreach($question as $qsn)
                        <?php 
                        if(!empty($qsn->subqsn)){
                            $subquestions = $qsn->subqsn;
                        }else{
                            $subquestions = array();
                        }
                        if(isset($qsn->question_type)){
                            $question_type = $qsn->question_type;
                        }else{
                            $question_type = '';
                        }
                        ?>
                        <div>
                        <div class="form-group row qsnrow">
                                    <label class="col-sm-2 col-form-label"> Question): <span
                                    class="mandatory_field">*</span> </label>
                                    <div class="col-sm-9">  
                                        <div class="form-group row">
                                        <div class="col-sm-12">
                                        <input class="form-control qsninput" type="text" name="qsn[]" index="{{$i}}" value="{{$qsn->question}}">
                                        
                                        
                                        </div>
                                            
                                        </div>
                                    </div>
                                    @if($i==0)
                                    <div class="col-sm-1">
                                        <div style="margin-top: 7px;"><i title="Add Question" id="add-subqsn"
                                        class="fa fa-plus-circle" aria-hidden="true"></i></div>
                                    </div>  
                                    @else
                                    <div class="col-lg-1">
                                        <div style="margin-top: 7px;">
                                        <i class="remove-answer fa fa-times" aria-hidden="true"></i>
                                        </div>
                                    </div> 
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"> Type of Question: <span
                                    class="mandatory_field">*</span> </label>
                                    <div class="col-sm-9">  
                                        <div class="form-group row">
                                        <div class="col-sm-12">
                                        <label class="radio-inline"><input type="radio" <?php if($question_type == 'subqsn'){ echo 'checked'; } ?> class="question_type" value="1" name="question_type[{{$i}}][]"  data-error="#errNm2"> Sub Questions</label>
                                        <label class="radio-inline"><input type="radio" <?php if($question_type == 'multiple_question'){ echo 'checked'; } ?> class="question_type" value="2" name="question_type[{{$i}}][]"> Multiple Choise</label>
                                        <label class="radio-inline"><input type="radio" <?php if($question_type == 'single_input'){ echo 'checked'; } ?> class="question_type" value="3" name="question_type[{{$i}}][]"> Single Input</label>
                                        <label class="radio-inline"><input type="radio" <?php if($question_type == 'textarea_input'){ echo 'checked'; } ?> class="question_type" value="4" name="question_type[{{$i}}][]"> Textarea Input</label> 
                                        <label class="radio-inline"><input type="radio" <?php if($question_type == 'fill_in_blank'){ echo 'checked'; } ?> class="question_type" value="5" name="question_type[{{$i}}][]"> Fill In Blank</label> 
                                        <span id="errNm2"></span>
                                        <div class="col-sm-12 subqsn">
                                        <?php if($question_type == 'subqsn'){ ?>
                                        <a href="javascript:void(0);" style="float: right;padding: 5px;text-decoration: underline;" class="add-sub-qsn">Add Sub Question</a>
                                            <div class="col-sm-12 sub-qsn-section" style="display: inline-block;">

                                                @if(count($subquestions)>0)
                                                    @foreach($subquestions as $subqsn)
                                                    <div style="margin-top:40px;" class="subqsnrow row ">
                                                    <label class="col-sm-2 col-form-label">Sub Qsn)</label>
                                                        <div class="col-sm-9">
                                                            <input class="form-control" type="text" name="subqsn[{{$i}}][]" value="{{$subqsn}}">
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <div style="margin-top: 7px;">
                                                                <i class="remove-subqsn fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @endif

                                            </div>
                                        <?php }else if($question_type == 'multiple_question'){ ?>
                                            <div class="subqsnrow">
                                            @if(count($subquestions)>0)
                                                    @foreach($subquestions as $subqsn)
                                            <div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" value="{{$subqsn}}" name="multiple_question[{{$i}}][]"></div></div><br />
                                               
                                            
                                            @endforeach
                                                @endif
                                                </div>
                                        <?php }else if($question_type == 'single_input'){ ?>
                                            <div class="subqsnrow">
                                            @if(count($subquestions)>0)
                                                    @foreach($subquestions as $subqsn)
                                                    <div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" name="single_input[{{$i}}][]" readonly></div></div>
                                               
                                            
                                            @endforeach
                                                @endif
                                                </div>
                                        <?php }else if($question_type == 'textarea_input'){ ?>
                                            <div class="subqsnrow">
                                            @if(count($subquestions)>0)
                                                    @foreach($subquestions as $subqsn)
                                                    <div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><textarea class="form-control" type="text" name="textarea_input[{{$i}}][]" readonly></textarea></div></div>
                                               
                                            
                                            @endforeach
                                                @endif
                                                </div>
                                        <?php }else if($question_type == 'fill_in_blank'){ ?>
                                            <div class="subqsnrow">
                                            @if(count($subquestions)>0)
                                                    @foreach($subquestions as $subqsn)
                                                    <div class="row"><label class="col-sm-2 col-form-label">Qsn)</label><div class="col-sm-9" ><textarea class="form-control" type="text" name="fill_in_blank[{{$i}}][]">{{$subqsn}}</textarea></div></div>
                                               
                                            
                                            @endforeach
                                                @endif
                                                </div>
                                        <?php } ?>
                                        </div>
                                        </div>
                                       
                                        
                                        </div>
                                    </div> 
                                </div>
                                </div>
                                <?php $i++; ?>
                        @endforeach
                        @endif
                        <div class="form-group row">
                            <div id="answer-section" class="col-sm-12"></div>
                        </div>
                        {!! csrf_field() !!}
                        <br>
                        <button type="submit" class="btn btn-primary" id="btn_save_school">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $("#add-questionnaire").validate({
        rules: {
            survey_name: "required",
            'qsn[]': "required", 
            'question_type[0][]': "required"
        },
        messages: {
            survey_name: "Please fill the survey",
            'qsn[]': "Please fill the question.",
            'question_type[0][]': "Please select question type"
        },
        errorPlacement: function(error, element) {
        var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    $('#add-subqsn').click(function() {
        var qsnlength = $('.qsnrow').length;
        if ($('.qsnrow').length < 10) {
            var answerHtml =
                '<div style="margin-top:7px;"><div class="form-group qsnrow row "><label class="col-sm-2 col-form-label">Question)</label><div class="col-sm-9" ><input class="form-control qsninput" index="'+qsnlength+'" type="text" name="qsn[]"></div><div class="col-lg-1"><div style="margin-top: 7px;"><i class="remove-answer fa fa-times" aria-hidden="true"></i></div></div></div><div class="form-group row">\
                                    <label class="col-sm-2 col-form-label"> Type of Question: <span\
                                    class="mandatory_field">*</span> </label>\
                                    <div class="col-sm-9">\
                                        <div class="form-group row">\
                                        <div class="col-sm-12">\
                                        <label class="radio-inline"><input type="radio"  class="question_type" value="1" name="question_type['+qsnlength+'][]"> Sub Questions</label>\
                                        <label class="radio-inline"><input type="radio"  class="question_type" value="2" name="question_type['+qsnlength+'][]"> Multiple Choise</label>\
                                        <label class="radio-inline"><input type="radio"  class="question_type" value="3" name="question_type['+qsnlength+'][]"> Single Input</label>\
                                        <label class="radio-inline"><input type="radio"  class="question_type" value="4" name="question_type['+qsnlength+'][]"> Textarea Input</label>\
                                        <label class="radio-inline"><input type="radio"  class="question_type" value="5" name="question_type['+qsnlength+'][]"> Fill In Blank</label><div class="col-sm-12 subqsn"></div></div></div></div></div></div>';
            $('#answer-section').append(answerHtml);
        }
    });

    var i = 1;
    $(document).on('click', '.add-sub-qsn', function() {
      // var index = $(this).prev('.qsninput').attr('index');
      console.log($(this).parent().parent().parent().parent().parent().prev('div.form-group1.row.qsnrow'));
      var index = $(this).parent().parent().parent().parent().parent().prev('div.qsnrow ').find('.qsninput').attr('index');
        if ($(this).next('.subqsnrow').length < 5) {
            var subqsnHtml =
                '<div style="margin-top:40px;" class="subqsnrow row "><label class="col-sm-2 col-form-label">Sub Qsn)</label><div class="col-sm-9" ><input class="form-control" type="text" name="subqsn['+index+'][]"></div><div class="col-lg-1"><div style="margin-top: 7px;"><i class="remove-subqsn fa fa-times" aria-hidden="true"></i></div></div></div>';
            $(this).next('.sub-qsn-section').append(subqsnHtml);
            i = i+1;
        }
    });

    $("body").on("click", ".remove-answer", function() {
        $(this).parent().parent().parent().parent().remove();
    });

    $("body").on("click", ".remove-subqsn", function() {
        $(this).parent().parent().parent().remove();
    });
    

    $("body").on("click", ".question_type", function() {
        var index = $(this).parent().parent().parent().parent().parent().prev('div.qsnrow ').find('.qsninput').attr('index');
        var type_val = this.value;
        if(type_val == 1){
            var subqsnHtml = '<a href="javascript:void(0);" style="float: right;padding: 5px;text-decoration: underline;" class="add-sub-qsn">Add Sub Question</a><div class="col-sm-12 sub-qsn-section" style="display: inline-block;"></div>';
            $(this).parent().parent('div.col-sm-12').closest('div.col-sm-12').children('.subqsn').html(subqsnHtml);
        }else if(type_val == 2){
            $(this).parent().parent('div.col-sm-12').closest('div.col-sm-12').children('.subqsn').html('');
            var subqsnHtml =
                '<div class="subqsnrow"><div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" name="multiple_question['+index+'][]"></div></div><br />\
                <div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" name="multiple_question['+index+'][]"></div></div><br />\
                <div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" name="multiple_question['+index+'][]"></div></div><br />\
                <div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" name="multiple_question['+index+'][]"></div></div><br />\
                </div>';
            $(this).parent().parent('div.col-sm-12').closest('div.col-sm-12').children('.subqsn').html(subqsnHtml);
        }else if(type_val == 3){
            var subqsnHtml =
                '<div class="subqsnrow"><div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><input class="form-control" type="text" name="single_input['+index+'][]" readonly></div></div></div>';
                $(this).parent().parent('div.col-sm-12').closest('div.col-sm-12').children('.subqsn').html(subqsnHtml);
        }
        else if(type_val == 4){
            var subqsnHtml =
                '<div class="subqsnrow"><div class="row"><label class="col-sm-2 col-form-label">Ans)</label><div class="col-sm-9" ><textarea class="form-control" type="text" name="textarea_input['+index+'][]" readonly></textarea></div></div></div>';
                $(this).parent().parent('div.col-sm-12').closest('div.col-sm-12').children('.subqsn').html(subqsnHtml);
        }
        else if(type_val == 5){
            var subqsnHtml =
                '<div class="subqsnrow"><div class="row"><label class="col-sm-2 col-form-label">Qsn)</label><div class="col-sm-9" ><textarea class="form-control" type="text" name="fill_in_blank['+index+'][]"></textarea></div></div></div>';
                $(this).parent().parent('div.col-sm-12').closest('div.col-sm-12').children('.subqsn').html(subqsnHtml);
        }
    });

    

});
</script>
@endsection