@extends('layouts.app')

@section('content')
<div class="page-body">
    <!-- Basic table card start -->
    @can('survey_create')
    <p>
        <a class="btn btn-success" href="{{ url('admin/createsurvey') }}" class="">Create</a>
        
    </p>
@endcan
@if (Session::has('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('success') }}</strong>
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('error') }}</strong>
</div>
@endif
    <div class="card">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="card-header">
    <h5>Survey Listing</h5>
</div>

<?php //echo '<pre>'; print_r($surveys); die; ?>
    <!-- Main content -->
    <div class="card-block table-border-style">
         <div class="table-responsive">
        <!-- Main row -->
            <table class="table table-bordered table-striped datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Survey</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            @foreach($surveys as $svs)
                           
                            <tr class="odd gradeX row_{{$svs->id}}">
                                <td>{{$i}}</td>
                               @if(Auth::user()->role->pluck('id')->toArray()[0] == 3)
                                <td>@if(!empty($svs->tour_id)) Tour Record # {{ $svs->tour_id }} from Tour Year {{ $svs->year }} @else 'NA' @endif</td>
                               @else
                                <td>{{ $svs->survey_name }}</td> 
                               @endif 
                                <td class="action">
                                    @can('survey_feedback') <a href="{{ url('admin/feedback/'.$svs->id) }}" class="feedback_survey"><i class="fas fa-poll"></i></a> @endcan
                                    @can('survey_edit') <a href="{{ url('admin/survey/edit/'.$svs->id) }}" class="edit_sub"><i class="ti-pencil"></i></a> @endcan
                                    @can('survey_delete') <a href="javascript:void(0);" id="{{$svs->id}}" class="delete_ques"><i class="ti-trash"></i></a> @endcan
                                </td>

                            </tr>
                            <?php 
									$i++;
								?>
                            @endforeach


                        </tbody>
                    </table>
             
     
        <!-- /.row (main row) -->
        </div>
    </div>
</div>
<!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function() {
    $('body').on('click', '.delete_ques', function() {
        var subid = $(this).attr('id');
        var r = confirm("Do you want to delete?");
        if (r == true) {
            $.ajax({
                type: "GET",
                url: "{{ url('admin/deletesurvey') }}" + '/' + subid,
                success: function(response) {
                    if (response == 'success') {
                        //location.reload();

                        $('.row_'+subid).css('display', 'none');
                    }
                }
            });
        }
    });
});
</script>
@endsection