@extends('layouts.app')

@section('content')
<div class="page-body">
    <div class="row">
        <h3 class="page-title">Profile</h3>
        <div class="col-sm-12">
            <?php //echo '<pre>'; print_r($user_info); die; ?>
            {!! Form::open(['method' => 'POST', 'class'=>'form form-vertical', 'enctype'=>'multipart/form-data']) !!}

            <div class="panel-heading">
              
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="card" style="width:100%">
                        <div class="card-block">
                        <div class="form-group row">
                                <div class="col-sm-4 text-center">
                                <div class="kv-avatar">
                                    <div class="file-loading">
                                    {!! Form::file('profilepic', old('profilepic'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>    
                                    <div class="kv-avatar-hint">
                                        <small>Select file < 500 KB</small>
                                    </div>
                                </div>
                              
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> {!! Form::label('name',
                                    trans('global.users.fields.name').'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::text('username', $user_info->name ?? '', ['class' => 'form-control name', 'placeholder' => '',
                                    'required'=> ''])
                                    !!}
                                </div>
                               
                            </div>
                            <div class="form-group row">

                                <label class="col-sm-2 col-form-label"> {!! Form::label('email',
                                    trans('global.users.fields.email').'*', ['class' =>
                                    'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::email('email', $user_info->email ?? '', ['class' => 'form-control email', 'placeholder' =>
                                    '',
                                    'required' =>
                                    '', 'readonly'=>'']) !!}
                                </div>
                               
                            </div>                          

                        </div>
                    </div>
                </div>
            </div>

            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<?php 
    $BASE_PATH = url('/file_upload.php');
    if(!empty($user_info->profile_image)){
        $ProfileImage = url("public/profilepic/".$user_info->profile_image);
    }else{
        $ProfileImage = url("public/assets/images/default-avatar-male.png");
    }
?>
<script>
    var basepath = '<?php echo $BASE_PATH; ?>';
    var preimage = '<img style="width:213px;" src="{{$ProfileImage}}" alt="Your Avatar">';
    $('input[type="file"]').fileinput({
        overwriteInitial: true,
        maxFileSize: 500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fa fa-folder-open"></i>',
        removeIcon: '<i class="fa fa-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: preimage,
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $(document).ready(function(){
        $(".form").validate({
            rules: {
                'username': "required",
                'email': "required"
            },
            messages: {
                'username': "Name is required",
                'email': "Email is required."
            }
        });
    });
</script>
<style>
    .kv-file-content img{width:213px;}
    .file-zoom-content img{width:100%}
</style>
@stop