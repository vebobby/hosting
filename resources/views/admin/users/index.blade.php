@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<div class="page-body">
    <!-- Basic table card start -->
    @can('user_create')
<p>
    <a class="btn btn-success" href="{{ route('admin.users.create') }}">Create</a>

</p>
@endcan  
@if (Session::has('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('success') }}</strong>
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('error') }}</strong>
</div>
@endif
<div class="card">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="card-header">
    <h5>Users Listing</h5>
</div>



<div class="card-block table-border-style">
         <div class="table-responsive">
                <table
                    class="table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }}">
                    <thead>
                        <tr>
                            <th>@lang('global.users.fields.name')</th>
                            <th>@lang('global.users.fields.email')</th>
                            <th>@lang('global.users.fields.role')</th>
                            <th>&nbsp;</th>

                        </tr>
                    </thead>

                    <tbody>
                        @if (count($users) > 0)
                        @foreach ($users as $user)
                        <tr data-entry-id="{{ $user->id }}">
                           

                            <td field-key='name'>{{ $user->name }}</td>
                            <td field-key='email'>{{ $user->email }}</td>
                            <td field-key='role'>
                                @foreach ($user->role as $singleRole)
                                <span class="label label-info label-many">{{ $singleRole->title }}</span>
                                @endforeach
                            </td>
                            <td>
							<div class="action">
                                @can('user_view')
                                <a href="{{ route('admin.users.show',[$user->id]) }}"
                                    class="bob_no_btn"><i class="ti-view-list"></i></a>
									
                                @endcan
                                @can('user_edit')
                                <a href="{{ route('admin.users.edit',[$user->id]) }}"
                                    class="bob_no_btn"><i class="ti-pencil"></i></a>
									
                                @endcan
                                @can('user_delete')	
                                {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['admin.users.destroy', $user->id])) !!}
                                {!! Form::button('<i class="ti-trash"></i>', array('type' => 'submit','class' => 'bob_no_btn')) !!}
                                {!! Form::close() !!}
                                @endcan
							</div>
                            </td>

                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@stop

@section('javascript') 
    <script>
        @can('user_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
        @endcan

    </script>
@endsection