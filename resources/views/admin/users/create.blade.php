@extends('layouts.app')
@section('content')
<div class="page-body">
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Create user</h5>
                </div>
                <div class="card-block">
            {!! Form::open(['method' => 'POST', 'route' => ['admin.users.store']]) !!}


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> {!! Form::label('name',
                                    trans('global.users.fields.name').'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '',
                                    'required'
                                    => ''])
                                    !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                                @endif
                            </div>
                            <div class="form-group row">

                                <label class="col-sm-2 col-form-label"> {!! Form::label('email',
                                    trans('global.users.fields.email').'*', ['class' =>
                                    'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' =>
                                    '',
                                    'required' =>
                                    '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('email'))
                                <p class="help-block">
                                    {{ $errors->first('email') }}
                                </p>
                                @endif

                            </div>
                            <div class="form-group row">

                                <label class="col-sm-2 col-form-label"> {!! Form::label('password',
                                    trans('global.users.fields.password').'*', ['class' =>
                                    'control-label']) !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '',
                                    'required'
                                    =>
                                    '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('password'))
                                <p class="help-block">
                                    {{ $errors->first('password') }}
                                </p>
                                @endif

                            </div>
                            <div class="form-group row">

                                <label class="col-sm-2 col-form-label"> {!! Form::label('role',
                                    trans('global.users.fields.role').'*', ['class' => 'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::select('role[]', $roles, old('role'), ['class' => 'form-control select2',
                                    'multiple'
                                    =>
                                    'multiple', 'required' => '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('role'))
                                <p class="help-block">
                                    {{ $errors->first('role') }}
                                </p>
                                @endif
                            </div>

          

            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
if($('.select2 :selected').val() == 3){
		$('.select2 option[value="1"]').attr('disabled','disabled');
		$('.select2 option[value="2"]').attr('disabled','disabled');
	}
	if($('.select2 :selected').val() == 1){
			$('.select2 option[value="3"]').attr('disabled','disabled');
	}
	if($('.select2 :selected').val() == 2){
		$('.select2 option[value="3"]').attr('disabled','disabled');
	}
	$('.select2').change(function () {
		$.each(this.options, function (i, item) {
            console.log(item);
                if(item.value == 3){
                    if (item.selected) {
                        $('.select2 option[value="1"]').attr('disabled','disabled');
                        $('.select2 option[value="2"]').attr('disabled','disabled');
                        return false;
                    }else{
                        $('.select2 option[value="1"]').removeAttr('disabled');
                        $('.select2 option[value="2"]').removeAttr('disabled');
                    }  
                }
                if(item.value == 1){
                    if (item.selected) {
                        $('.select2 option[value="3"]').attr('disabled','disabled');
                        return false;
                    }else{
                        $('.select2 option[value="3"]').removeAttr('disabled');
                    }
                        
                }
                if(item.value == 2){
                    if (item.selected) {
                        $('.select2 option[value="3"]').attr('disabled','disabled');
                        return false;	
                    }else{
                        $('.select2 option[value="3"]').removeAttr('disabled');
                    }
                    
                }		
			});
			$('.select2').select2();
        });
    });
</script>
@stop