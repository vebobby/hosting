@extends('layouts.app')

@section('content')
<div class="page-body">
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Edit Permission</h5>
                </div>
                <div class="card-block">
            {!! Form::model($permission, ['method' => 'PUT', 'route' => ['admin.permissions.update', $permission->id]])
            !!}
                            <div class="form-group row">

                                <label class="col-sm-2 col-form-label"> {!! Form::label('title',
                                    trans('global.permissions.fields.title').'*', ['class' =>
                                    'control-label'])
                                    !!}</label>
                                <div class="col-sm-10">
                                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' =>
                                    '',
                                    'required' =>
                                    '']) !!}
                                </div>
                                <p class="help-block"></p>
                                @if($errors->has('title'))
                                <p class="help-block">
                                    {{ $errors->first('title') }}
                                </p>
                                @endif
                            </div>
                        
            {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
            </div>
    </div>
</div>
</div>
</div>
@stop