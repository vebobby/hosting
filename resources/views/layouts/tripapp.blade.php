<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
</head>

        <!-- Pre-loader start -->
        <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            @include('trip.topbar')
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                     @include('trip.sidebar')

        <!-- Content Wrapper. Contains page content -->
                    <div class="pcoded-content">
                            <div class="pcoded-inner-content">
                                <!-- Main-body start -->
                                <div class="main-body">
                                    <div class="page-wrapper">
                                    @yield('content')
                                    </div>
                                </div>
                            <!-- Main-body end -->

                                <div id="styleSelector">

                                </div>
                            </div>
                    </div>
                 </div>
            </div>
        </div>


    {!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
    <button type="submit">Logout</button>
    {!! Form::close() !!}

    @include('partials.javascripts')
	 </div>
</body>

</html>