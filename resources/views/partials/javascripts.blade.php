<script>
window.deleteButtonTrans = '{{ trans("global.app_delete_selected") }}';
window.copyButtonTrans = '{{ trans("global.app_copy") }}';
window.csvButtonTrans = '{{ trans("global.app_csv") }}';
window.excelButtonTrans = '{{ trans("global.app_excel") }}';
window.pdfButtonTrans = '{{ trans("global.app_pdf") }}';
window.printButtonTrans = '{{ trans("global.app_print") }}';
window.colvisButtonTrans = '{{ trans("global.app_colvis") }}';
</script>





<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ url('public/assets/js/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{ url('public/assets/js/modernizr/modernizr.js') }}"></script>
<!-- am chart -->
<script src="{{ url('public/assets/pages/widget/amchart/amcharts.min.js') }}"></script>
<script src="{{ url('public/assets/pages/widget/amchart/serial.min.js') }}"></script>
<!-- Chart js -->
<script type="text/javascript" src="{{ url('public/assets/js/chart.js/Chart.js') }}"></script>
<!-- Todo js -->
<script type="text/javascript " src="{{ url('public/assets/pages/todo/todo.js') }}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ url('public/assets/pages/dashboard/custom-dashboard.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/assets/js/script.js') }}"></script>
<script type="text/javascript " src="{{ url('public/assets/js/SmoothScroll.js') }}"></script>
<script src="{{ url('public/assets/js/pcoded.min.js') }}"></script>
<script src="{{ url('public/assets/js/vartical-demo.js') }}"></script>
<script src="{{ url('public/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<script src="{{ url('public/assets/js/jquery.validate.min.js') }}"></script>
<script>
window._token = '{{ csrf_token() }}';
</script>



 <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<?php /* <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script> */ ?>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="{{ url('public/adminlte/js/bootstrap.min.js') }}"></script>
<script src="{{ url('public/adminlte/js/select2.full.min.js') }}"></script>
<script src="{{ url('public/adminlte/js/main.js') }}"></script>

<script src="{{ url('public/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('public/adminlte/plugins/fastclick/fastclick.js') }}"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<!--<script>tinymce.init({selector:'textarea'});</script>-->
<script src="{{ url('public/adminlte/js/app.min.js') }}"></script> 
<script src='https://kit.fontawesome.com/a076d05399.js'></script>



<script>
$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "url": "http://cdn.datatables.net/plug-ins/1.10.16/i18n/English.json"
    }
});
</script>

@yield('javascript')