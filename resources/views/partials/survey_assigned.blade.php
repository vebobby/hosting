
<div class="media">
    <img class="d-flex align-self-center img-radius" src="{{url('public/assets/images/survey.png')}}"
        alt="Generic placeholder image">
    <div class="media-body">
        <h5 class="notification-user">New Survey Assigned</h5>
        <p class="notification-msg">{{$notification->data['survey']['creator']['name']}} has assigned a survey <b><a href="{{ url('admin/feedback/'.$notification->data['survey']['TourSurvey']['id'].'/'.$notification->id) }}" >{{$notification->data['survey']['survey']['survey_name']}}</a></b> from tour record <b>{{$notification->data['survey']['TourSurvey']['tour_id']}}</b></p>
        <span class="notification-time">30 minutes ago</span>
    </div>
</div>